<?php
    include 'header.php';
    $wrongemail = false;

    if(isset($_POST["email"])){
      /** @var \pmill\AwsCognito\CognitoClient $client */
      $client = require(__DIR__ . '/cognito/bootstrap.php');
      $password = $_POST["password"];
      $email = $_POST["email"];
      $username = $email;
      try{
        $client->registerUser($username, $password, [
            'email' => $email,
        ]);
        header('Location: confirmation.php?username='.$username);
      }catch(Exception $e) {
        $wrongemail = true;
      }



    }

?>
<body>
<div id="content">

    <div class="container">
    <span><h1>Register</h1></span>
    <?php if($wrongemail == true){echo '<blockquote class="white-text text-darken-2" style="border-left: 5px solid rgb(255, 0, 54);">
    <h6>Email already in use.</h6>';$wrongemail = false;} ?>
    <div class="row">
      <form class="col s12" action="registration.php" method="POST" enctype="multipart/form-data">
        <blockquote>
          The password must fulfill these requirements:<br>
          · Include upper and lower case letters<br>
          · Include a numerical character<br>
          · Include a special character<br>
          · Minimum length of 8<br>
        </blockquote>
        <div class="row">
          <div class="input-field col s12">
            <input id="email" type="email" name="email" class="validate">
            <label for="email">Email</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="password" name="password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,10}" type="password" class="validate">
            <label for="password">Password</label>
          </div>
        </div>
        <div class="row">
          <button type="submit" class="btn waves-effect waves-light green" name="action">Submit
            <i class="material-icons right">send</i>
          </button>
        </div>
      </form>
    </div>
    <a class="waves-effect waves-light btn red lighten-2" href="login.php"><i class="material-icons right">person</i>log in</a>

    </div>
</div>

<script src='js/materialize.min.js'></script>
<script  src="js/index.js"></script>

</body>
<br><br><br>
<?php include 'footer.php'; ?>
