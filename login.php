<?php
use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;
    include 'header.php';

    $wrongpassword = false;
    if(isset($_POST["email"])){



      /** @var \pmill\AwsCognito\CognitoClient $client */
      $client = require(__DIR__ . '/cognito/bootstrap.php');
      $username = $_POST["email"];
      $password = $_POST["password"];
      try {
        $authenticationResponse = $client->authenticate($username, $password);
        session_start();
        $_SESSION['username'] = $username;

        require 'vendor/autoload.php';

        $sdk = new Aws\Sdk([
            'region'   => 'us-east-2',
            'version'  => 'latest'
          ]);

        $dynamodb = $sdk->createDynamoDb();
  $marshaler = new Marshaler();

  $tableName = 'UsersPremium';

  $eav = $marshaler->marshalJson('
      {
          ":email": "'.$_POST["email"].'"
      }
  ');

  //
  $params = [
      'TableName' => $tableName,
      'KeyConditionExpression' => 'Email = :email',
      'ExpressionAttributeValues'=> $eav
  ];


  try {
      $result = $dynamodb->query($params);
      if(!empty($result['Items'])){

        $item = $marshaler->unmarshalItem($result['Items'][0]);
        $_SESSION['services'] =$item['Preferences'];
        $_SESSION['premium'] =True;

      }

  } catch (DynamoDbException $e) {
      echo "Unable to query:\n";
      echo $e->getMessage() . "\n";
  }

  $dynamodb = $sdk->createDynamoDb();
$marshaler = new Marshaler();

$tableName = 'Tenants';

$eav = $marshaler->marshalJson('
{
    ":email": "'.$_SESSION['username'].'"
}
');

//
$params = [
'TableName' => $tableName,
'KeyConditionExpression' => 'Email = :email',
'ExpressionAttributeValues'=> $eav
];


try {
$result = $dynamodb->query($params);
if(!empty($result['Items'])){
  $item = $marshaler->unmarshalItem($result['Items'][0]);
  $_SESSION['tenant'] =True;
}

} catch (DynamoDbException $e) {
echo "Unable to query:\n";
echo $e->getMessage() . "\n";
}

        header('Location: index.php');
      }

      catch(Exception $e) {
        $wrongpassword = true;
      }

    }

?>


<body >
    <div class="container">
    <span><h1>Log In</h1></span>
    <?php if($wrongpassword == true){
      echo '<blockquote style="border-left: 5px solid rgb(255, 0, 54);">
    <h6>Wrong password.</h6></blockquote>';$wrongpassword = false;} ?>

    <div class="row">
      <form class="col s12" action="login.php" id="logform" method="POST">
        <div class="row">
          <div class="input-field col s12">
            <input id="email" type="email" name="email" class="validate">
            <label for="email">Email</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="password" type="password" name="password" class="validate">
            <label for="password">Password</label>
          </div>
        </div>
        <div class="row">
          <button type="submit" form="logform" class="btn waves-effect waves-light green" name="action">Submit
            <i class="material-icons right">send</i>
          </button>
        </div>
      </form>
    </div>
    <a class="waves-effect waves-light btn red lighten-2" href="registration.php"><i class="material-icons right">person_add</i>register</a>

    </div>
    <br><br><br>

    <script src='js/materialize.min.js'></script>
    <script  src="js/index.js"></script>
  </body>
  <?php include 'footer.php'; ?>
