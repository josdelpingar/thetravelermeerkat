<?php
  require 'vendor/autoload.php';

  session_start();

  if(!isset($_SESSION['username'])) {
      header('Location: login.php');
  }

  use Aws\DynamoDb\Exception\DynamoDbException;
  use Aws\DynamoDb\Marshaler;

  $sdk = new Aws\Sdk([
      'region'   => 'us-east-2',
      'version'  => 'latest'
    ]);
    $email = $_SESSION['username'];


   $dynamodb = $sdk->createDynamoDb();
   $marshaler = new Marshaler();

   $item = $marshaler->marshalJson('
            {
               "Email": "' . $email . '",
                "Users": 0

            }
        ');
 $params = ['TableName' => 'Tenants','Item' => $item];

        try {
          $result = $dynamodb->putItem($params);
          $_SESSION['tenant'] =True;
          header("Location: index.php");

        } catch (DynamoDbException $e) {
          echo "Unable to add item:\n";
          echo $e->getMessage() . "\n";
        }

?>
