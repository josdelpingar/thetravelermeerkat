<?php
    include 'header.php';
    $wrongcode = false;
    /** @var \pmill\AwsCognito\CognitoClient $client */

    if(isset($_POST["code"])){
      $client = require(__DIR__ . '/cognito/bootstrap.php');
      $confirmationCode = $_POST["code"];
      $username = $_POST["username"];

      try{
      $client->confirmUserRegistration($confirmationCode, $username);
      session_start();
      header('Location: index.php');
      }catch(Exception $e) {
        $wrongcode = true;
      }
    }

?>

    <body>
        <div class="container">
        <h2>Enter the received code:</h2>
        <?php if($wrongcode == true){

          echo '<blockquote><h6>The code is incorrect.</h6></blockquote>';
          $wrongcode = false;
        } ?>
        <div class="row">
          <form class="col s12" action="confirmation.php?username=<?php echo $_GET['username']?>" id="confform" enctype="multipart/form-data" method="POST">

            <div class="row">
              <div class="input-field col s12">
                <input id="code" type="text" name="code">
                <label for="code">Code</label>
              </div>
            </div>
            <input type="hidden" name="username" id="username" value='<?php echo $_GET['username']; ?>'/>
            <div class="row">
              <button type="submit" form="confform" class="btn waves-effect waves-light" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </div>
          </form>
        </div>
        </div>
        <script src='js/materialize.min.js'></script>
        <br><br><br><br><br><br><br><br><br>
      </body>
<?php include 'footer.php'; ?>
