<?php
  require 'vendor/autoload.php';

  session_start();

  if(!isset($_SESSION['username'])) {
      header('Location: login.php');
  }

  use Aws\DynamoDb\Exception\DynamoDbException;
  use Aws\DynamoDb\Marshaler;

  $sdk = new Aws\Sdk([
      'region'   => 'us-east-2',
      'version'  => 'latest'
    ]);


    if($_POST['f']=='on'){
      $f = 1;
    }else{
      $f = 0;
    }
    if($_POST['wha']=='on'){
      $wha = 1;
    }else{
      $wha = 0;
    }
    if($_POST['tw']=='on'){
      $tw = 1;
    }else{
      $tw = 0;
    }
    if($_POST['in']=='on'){
      $in = 1;
    }else{
      $in = 0;
    }
    if($_POST['sn']=='on'){
      $sn = 1;
    }else{
      $sn = 0;
    }
    if($_POST['tg']=='on'){
      $tg = 1;
    }else{
      $tg = 0;
    }
    if($_POST['yt']=='on'){
      $yt = 1;
    }else{
      $yt = 0;
    }
    if($_POST['net']=='on'){
      $net = 1;
    }else{
      $net = 0;
    }
    if($_POST['hu']=='on'){
      $hu = 1;
    }else{
      $hu = 0;
    }
    if($_POST['hbo']=='on'){
      $hbo= 1;
    }else{
      $hbo = 0;
    }
    if($_POST['sp']=='on'){
      $sp = 1;
    }else{
      $sp = 0;
    }
    if($_POST['am']=='on'){
      $am = 1;
    }else{
      $am = 0;
    }
    if($_POST['ti']=='on'){
      $ti = 1;
    }else{
      $ti = 0;
    }
    if($_POST['g']=='on'){
      $g = 1;
    }else{
      $g = 0;
    }
    if($_POST['gma']=='on'){
      $gma = 1;
    }else{
      $gma = 0;
    }
    if($_POST['gm']=='on'){
      $gm = 1;
    }else{
      $gm = 0;
    }
    if($_POST['gd']=='on'){
      $gd = 1;
    }else{
      $gd = 0;
    }
    if($_POST['gp']=='on'){
      $gp = 1;
    }else{
      $gp = 0;
    }
    if($_POST['vpn']=='on'){
      $vpn = 1;
    }else{
      $vpn = 0;
    }


    $dynamodb = $sdk->createDynamoDb();
    $marshaler = new Marshaler();
 //,"Tenant": "' . $tenant . '"
    $key = $marshaler->marshalJson('
             {
                "Email": "' . $_SESSION['username'] . '"

             }
         ');

         $eav = $marshaler->marshalJson('
             {
                 ":set": [ "'.$f.'", "'
                 .$in.'", "'
                 .$wha.'","'
                 .$tw.'","'
                 .$sn.'","'.$tg.'","'.$yt.'","'
                 .$net.'","'.$hu.'","'.$hbo.'","'
                 .$sp.'","'.$am.'","'.$ti.'","'
                 .$g.'","'.$gma.'","'.$gm.'","'.$gd.'","'.$gp.'","'.$vpn.'" ]
               }
         ');

         $params = [
             'TableName' => 'UsersPremium',
             'Key' => $key,
             'UpdateExpression' =>
                 'set Preferences = :set',
             'ExpressionAttributeValues'=> $eav,
             'ReturnValues' => 'UPDATED_NEW'
         ];

  //$params = ['TableName' => 'UsersPremium','Key' => $key];

         try {
           $result = $dynamodb->updateItem($params);
           header("Location: account.php");

         } catch (DynamoDbException $e) {
           echo "Unable to update item:\n";
           echo $e->getMessage() . "\n";
         }


?>
