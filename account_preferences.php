<?php
require 'vendor/autoload.php';
use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;

$sdk = new Aws\Sdk([
    'region'   => 'us-east-2',
    'version'  => 'latest'
  ]);

$dynamodb = $sdk->createDynamoDb();
$marshaler = new Marshaler();

$tableName = 'UsersPremium';

$eav = $marshaler->marshalJson('
{
  ":email": "'.$_SESSION['username'].'"
}
');

$params = [
'TableName' => $tableName,
'KeyConditionExpression' => 'Email = :email',
'ExpressionAttributeValues'=> $eav
];


try {
$result = $dynamodb->query($params);
if(!empty($result['Items'])){
$item = $marshaler->unmarshalItem($result['Items'][0]);
$_SESSION['services'] = $item['Preferences'];
}

} catch (DynamoDbException $e) {
echo "Unable to query:\n";
echo $e->getMessage() . "\n";
}
?>

<form action="updateservices.php" id="updateservices" method="POST" enctype="multipart/form-data">
  <blockquote>
<h6>Please check the services you use so we can warn you about the ones that won't be available in each country.</h6>
</blockquote>
  <h2>Services</h2>


  <div class="row">
  <div class="col s12 m3">
  <h6>Social Media</h6>
  <p>
    <input name="f" value="0" type="hidden">
    <label>
      <input type="checkbox" id="f" name="f" <?php if($item['Preferences'][0]==1) echo 'checked="checked"'; ?>/>
      <span>Facebook</span>
    </label>
  </p>
  <p>
    <input name="in" value="0" type="hidden">
    <label>
      <input type="checkbox" name="in" <?php if($item['Preferences'][1]==1) echo 'checked="checked"'; ?>/>
      <span>Instagram</span>
    </label>
  </p><p>
    <input name="wha" value="0" type="hidden">
    <label>
      <input type="checkbox" name="wha" <?php if($item['Preferences'][2]==1) echo 'checked="checked"'; ?>/>
      <span>WhatsApp</span>
    </label>
  </p>
  <p>
    <input name="tw" value="0" type="hidden">
    <label>
      <input type="checkbox" name="tw" <?php if($item['Preferences'][3]==1) echo 'checked="checked"'; ?>/>
      <span>Twitter</span>
    </label>
  </p>
  <p>
    <input name="sn" value="0" type="hidden">
    <label>
      <input type="checkbox" name="sn" <?php if($item['Preferences'][4]==1) echo 'checked="checked"' ?>/>
      <span>Snapchat</span>
    </label>
  </p>
  <p>
    <input name="tg" value="0" type="hidden">
    <label>
      <input type="checkbox" name="tg" <?php if($item['Preferences'][5]==1) echo 'checked="checked"' ?>/>
      <span>Telegram</span>
    </label>
  </p>
</div>
  <div class="col s12 m3">
  <h6>Video On-Demand</h6>
  <p>
    <input name="yt" value="0" type="hidden">
    <label>
      <input type="checkbox" name="yt" <?php if($item['Preferences'][6]==1) echo 'checked="checked"' ?>/>
      <span>YouTube</span>
    </label>
  </p>
   <p>
     <input name="net" value="0" type="hidden">
     <label>
       <input type="checkbox" name="net" <?php if($item['Preferences'][7]==1) echo 'checked="checked"' ?>/>
       <span>Netflix</span>
     </label>
   </p>
   <p>
     <input name="hu" value="0" type="hidden">
     <label>
       <input type="checkbox" name="hu" <?php if($item['Preferences'][8]==1) echo 'checked="checked"' ?>/>
       <span>Hulu</span>
     </label>
   </p>
   <p>
     <input name="hbo" value="0" type="hidden">
     <label>
       <input type="checkbox" name="hbo" <?php if($item['Preferences'][9]==1) echo 'checked="checked"' ?>/>
       <span>HBO</span>
     </label>
   </p>
 </div><div class="col s12 m3">
   <h6>Music On-Demand</h6>

   <p>
     <input name="sp" value="0" type="hidden">
     <label>
       <input type="checkbox" name="sp" <?php if($item['Preferences'][10]==1) echo 'checked="checked"' ?>/>
       <span>Spotify</span>
     </label>
   </p>
   <p>
     <input name="am" value="0" type="hidden">
     <label>
       <input type="checkbox" name="am" <?php if($item['Preferences'][11]==1) echo 'checked="checked"' ?>/>
       <span>Apple Music</span>
     </label>
   </p>
   <p>
     <input name="ti" value="0" type="hidden">
     <label>
       <input type="checkbox" name="ti" <?php if($item['Preferences'][12]==1) echo 'checked="checked"' ?>/>
       <span>TIDAL</span>
     </label>
   </p>
   </div>
   <div class="col s12 m3">
     <h6>Others</h6>
     <p>
       <input name="g" value="0" type="hidden">
       <label>
         <input type="checkbox" name="g" <?php if($item['Preferences'][13]==1) echo 'checked="checked"' ?>/>
         <span>Google</span>
       </label>
     </p>
     <p>
       <input name="gma" value="0" type="hidden">
       <label>
         <input type="checkbox" name="gma" <?php if($item['Preferences'][14]==1) echo 'checked="checked"' ?>/>
         <span>GMail</span>
       </label>
     </p>
     <p>
       <input name="gm" value="0" type="hidden">
       <label>
         <input type="checkbox" name="gm" <?php if($item['Preferences'][15]==1) echo 'checked="checked"' ?>/>
         <span>Google Maps</span>
       </label>
     </p>
     <p>
       <input name="gd" value="0" type="hidden">
       <label>
         <input type="checkbox" name="gd" <?php if($item['Preferences'][16]==1) echo 'checked="checked"' ?>/>
         <span>Google Drive</span>
       </label>
     </p>
     <p>
       <input name="gp" value="0" type="hidden">
       <label>
         <input type="checkbox" name="gp" <?php if($item['Preferences'][17]==1) echo 'checked="checked"' ?>/>
         <span>Google Play</span>
       </label>
     </p>
     <p>
       <input name="vpn" value="0" type="hidden">
       <label>
         <input type="checkbox" name="vpn"  <?php if($item['Preferences'][18]==1) echo 'checked="checked"' ?>/>
         <span>VPNs</span>
       </label>
     </p>

     <br/>
     </div>
     <button type="submit" form="updateservices" class="btn waves-effect waves-light" name="action">submit changes
       <i class="material-icons right">send</i>
     </button>
</form>
</div>
