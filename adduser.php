<?php
  require 'vendor/autoload.php';

  session_start();

  if(!isset($_SESSION['username'])) {
      header('Location: login.php');
  }

  use Aws\DynamoDb\Exception\DynamoDbException;
  use Aws\DynamoDb\Marshaler;

  $sdk = new Aws\Sdk([
      'region'   => 'us-east-2',
      'version'  => 'latest'
    ]);
    $tenant = $_SESSION['username'];
    $user = $_POST['newuser'];

   $dynamodb = $sdk->createDynamoDb();
   $marshaler = new Marshaler();

   $item = $marshaler->marshalJson('
            {
               "Email": "' . $user . '",
               "Tenant": "' . $tenant . '",
               "Preferences": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

            }
        ');
 $params = ['TableName' => 'UsersPremium','Item' => $item];

        try {
          $result = $dynamodb->putItem($params);
          if ($_SESSION['premium']!=True)
						{
              if($_SESSION['username']==$_POST['newuser']){
                $_SESSION['premium']=True;
              }

						}
          header("Location: account.php");

        } catch (DynamoDbException $e) {
          echo "Unable to add item:\n";
          echo $e->getMessage() . "\n";
        }

?>
