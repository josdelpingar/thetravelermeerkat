<?php
  require 'vendor/autoload.php';


  use Aws\DynamoDb\Exception\DynamoDbException;
  use Aws\DynamoDb\Marshaler;

  $sdk = new Aws\Sdk([
      'region'   => 'us-east-2',
      'version'  => 'latest'
    ]);

   $dynamodb = $sdk->createDynamoDb();
   $marshaler = new Marshaler();
   $params = ['TableName' => 'States'];

    try {
           $result = $dynamodb->scan($params);
           $europe = array();
           $asia = array();
           $northamerica = array();
           $southamerica = array();
           $africa = array();
           $oceania = array();

           foreach ($result['Items'] as $i) {

            $item = $marshaler->unmarshalItem($i);

            switch ($item["Continent"]) {
              case 0:
               $oceania[] = $item;
               break;
              case 1:
               $africa[] = $item;
               break;
              case 2:
               $southamerica[] = $item;
               break;
              case 3:
                $northamerica[] = $item;
                break;
              case 4:
                $asia[] = $item;
                break;
              case 5:
                $europe[] = $item;
                break;
           }

            }



         } catch (DynamoDbException $e) {
           echo '<li class="collection-item">It was not possible to retrieve the data</li>';

         }

?>
