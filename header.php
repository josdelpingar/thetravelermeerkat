<!doctype html>
  <html>
    <head>
      <link rel="shortcut icon" href="img/logo_dark.ico" />
      <link rel="icon" type="image/png" href="img/logo_dark.png" />
      <link rel="icon" href="img/logo_dark.png" type="image/x-icon">
      <link rel="shortcut icon" href="img/logo_dark.png" type="image/x-icon">
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <!--Let browser know website is optimized for mobile data-collapsible="accordion"-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>The Traveler Meerkat</title>
    </head>
    <body>
      <div class="navbar-fixed">
        <nav>
          <div class="nav-wrapper cyan">
              <a href="index.php" class="brand-logo"><img src="img/logo.png" style="width:60px;height:60px;"></a>
              <ul id="nav-mobile" class="hide-on-med-and-down">
              <li><img src="img/logo.png" style="width:60px;height:60px;"></li>
              <li>THE TRAVELER MEERKAT</li>
              </ul>
              <ul id="nav-mobile" class="right hide-on-med-and-down">
                <?php
                if(isset($_SESSION['username'])) {
                  echo  '<li><a href="logout.php"class="waves-effect waves-light white btn-small"><span class="red-text text-lighten-2 ">log out</span></a></li>';
                }
                ?>

              </ul>
          </div>
        </nav>
      </div>
    </body>

  </html>
