<?php
session_start();

if (!isset($_SESSION['username']))
{
    header('Location: login.php');
}

include 'header.php';

?>

<head>
  <link rel="shortcut icon" href="img/logo_dark.ico" />
  <link rel="icon" type="image/png" href="img/logo_dark.png" />
  <link rel="icon" href="img/logo_dark.png" type="image/x-icon">
  <link rel="shortcut icon" href="img/logo_dark.png" type="image/x-icon">
  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <!--Let browser know website is optimized for mobile data-collapsible="accordion"-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>The Traveler Meerkat</title>
  <style>
  .promo i {
    margin: 40px 0;
    color: #ee6e73;
    font-size: 7rem;
    display: block;
  }
  .promo-caption {
    font-size: 1.7rem;
    font-weight: 500;
    margin-top: 5px;
    margin-bottom: 0;
}
</style>

</head>


    <body class="grey lighten-4">

        <div class="container">
          <br/><br/><br/>
          <div class="row">

                  <div class="col s12 m4">
                    <div class="center promo promo-example">
                      <i class="material-icons">brush</i>
                      <p class="promo-caption">Personalized</p>
                      <p class="light center">Every user will have the possibility to choose the services they use for a more personal experience.</p>
                    </div>
                  </div>

                  <div class="col s12 m4">
                    <div class="center promo promo-example">
                      <i class="material-icons">looks_one</i>
                      <p class="promo-caption">One time payment</p>
                      <p class="light center">Pay once and enjoy our service forever.</p>
                    </div>
                  </div>
                  <div class="col s12 m4">
                    <div class="center promo promo-example">
                      <i class="material-icons">filter_8</i>
                      <p class="promo-caption">Up to 8 users</p>
                      <p class="light center">You don't have to pay for every new user, once you pay you can have up to 8 users for free.</p>
                    </div>
                  </div>
          </div>
          <div class="center-align">
          <a href="tenantreg.php"class="waves-effect waves-light btn-large red lighten-2"><span class="grey-text text-lighten-4 ">JOIN</span></a>
        </div>

<br/><br/><br/>
        </div>


    </body>

    <?php
    include 'footer.php';
    ?>
