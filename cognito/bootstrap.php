<?php
//require_once __DIR__ . '/../vendor/autoload.php';
require($_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php');
$config = require('config.php');
$aws = new \Aws\Sdk($config);
$cognitoClient = $aws->createCognitoIdentityProvider();
$client = new \pmill\AwsCognito\CognitoClient($cognitoClient);
$client->setAppClientId($config['app_client_id']);
$client->setAppClientSecret($config['app_client_secret']);
$client->setRegion($config['region']);
$client->setUserPoolId($config['user_pool_id']);
$client->downloadJwtWebKeys();
return $client;
?>
