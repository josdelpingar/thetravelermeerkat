<?php
session_start();

if (!isset($_SESSION['username']))
	{
	header('Location: login.php');
	}

include 'header.php';

?>

<html>

  <head>
    <link rel="shortcut icon" href="img/logo_dark.ico" />
    <link rel="icon" type="image/png" href="img/logo_dark.png" />
    <link rel="icon" href="img/logo_dark.png" type="image/x-icon">
    <link rel="shortcut icon" href="img/logo_dark.png" type="image/x-icon">
  </head>

  <body>
  <br /><br /><br />

    <div class="container">

<?php
include ('countries.php');
?>

      <ul class="collapsible popout">
 <li>
   <div class="collapsible-header">
     <i class="material-icons"><img src="img/europe.png" height="20" width="20"> </i>
     Europe
     <span class="new badge red accent-3" data-badge-caption="countries"><?php
echo count($europe); ?></span>
 </div>
   <div class="collapsible-body"><?php

foreach($europe as $item)
	{
	echo '<a class="collection-item" href="profile.php?Name=' . $item['Name'] . '&DemocracyIndex=' . $item['DemocracyIndex'] . '&Freedom=' . $item['Freedom'] . '&HomoLegalStatus=' . $item['HomoLegalStatus'] . '&HomoUnacceptable=' . $item['HomoUnacceptable'] . '&WomenDressCode=' . $item['WomenDressCode'] . '&WomenGenderGap=' . $item['WomenGenderGap'] . '&f=' . $item['Services'][0] . '&in=' . $item['Services'][1] . '&wha=' . $item['Services'][2] . '&tw=' . $item['Services'][3] . '&sn=' . $item['Services'][4] . '&tg=' . $item['Services'][5] . '&yt=' . $item['Services'][6] . '&net=' . $item['Services'][7] . '&hu=' . $item['Services'][8] . '&hbo=' . $item['Services'][9] . '&sp=' . $item['Services'][10] . '&am=' . $item['Services'][11] . '&ti=' . $item['Services'][12] . '&g=' . $item['Services'][13] . '&gma=' . $item['Services'][14] . '&gm=' . $item['Services'][15] . '&gd=' . $item['Services'][16] . '&gp=' . $item['Services'][17] . '&vpn=' . $item['Services'][18] . '&WomenSecurity=' . $item['WomenSecurity'] . '"><h6>' . $item['Flag'] . $item['Name'] . '</h6></a>';
	}

?></div>
 </li>
 <li>
   <div class="collapsible-header"><i class="material-icons"><img src="img/asia.png" height="20" width="20"> </i>Asia
   <span class="new badge orange accent-3" data-badge-caption="countries"><?php
echo count($asia); ?></span></div>
   <div class="collapsible-body"><?php

foreach($asia as $item)
	{
	echo '<a class="collection-item" href="profile.php?Name=' . $item['Name'] . '&DemocracyIndex=' . $item['DemocracyIndex'] . '&Freedom=' . $item['Freedom'] . '&HomoLegalStatus=' . $item['HomoLegalStatus'] . '&HomoUnacceptable=' . $item['HomoUnacceptable'] . '&WomenDressCode=' . $item['WomenDressCode'] . '&WomenGenderGap=' . $item['WomenGenderGap'] . '&f=' . $item['Services'][0] . '&in=' . $item['Services'][1] . '&wha=' . $item['Services'][2] . '&tw=' . $item['Services'][3] . '&sn=' . $item['Services'][4] . '&tg=' . $item['Services'][5] . '&yt=' . $item['Services'][6] . '&net=' . $item['Services'][7] . '&hu=' . $item['Services'][8] . '&hbo=' . $item['Services'][9] . '&sp=' . $item['Services'][10] . '&am=' . $item['Services'][11] . '&ti=' . $item['Services'][12] . '&g=' . $item['Services'][13] . '&gma=' . $item['Services'][14] . '&gm=' . $item['Services'][15] . '&gd=' . $item['Services'][16] . '&gp=' . $item['Services'][17] . '&vpn=' . $item['Services'][18] . '&WomenSecurity=' . $item['WomenSecurity'] . '"><h6>' . $item['Flag'] . $item['Name'] . '</h6></a>';
	}

?></div>
 </li>
 <li>
   <div class="collapsible-header"><i class="material-icons"><img src="img/northamerica.png" height="20" width="20"> </i>North America
   <span class="new badge amber accent-3" data-badge-caption="countries"><?php
echo count($northamerica); ?></span></div>
   <div class="collapsible-body"><?php

foreach($northamerica as $item)
	{
	echo '<a class="collection-item" href="profile.php?Name=' . $item['Name'] . '&DemocracyIndex=' . $item['DemocracyIndex'] . '&Freedom=' . $item['Freedom'] . '&HomoLegalStatus=' . $item['HomoLegalStatus'] . '&HomoUnacceptable=' . $item['HomoUnacceptable'] . '&WomenDressCode=' . $item['WomenDressCode'] . '&WomenGenderGap=' . $item['WomenGenderGap'] . '&f=' . $item['Services'][0] . '&in=' . $item['Services'][1] . '&wha=' . $item['Services'][2] . '&tw=' . $item['Services'][3] . '&sn=' . $item['Services'][4] . '&tg=' . $item['Services'][5] . '&yt=' . $item['Services'][6] . '&net=' . $item['Services'][7] . '&hu=' . $item['Services'][8] . '&hbo=' . $item['Services'][9] . '&sp=' . $item['Services'][10] . '&am=' . $item['Services'][11] . '&ti=' . $item['Services'][12] . '&g=' . $item['Services'][13] . '&gma=' . $item['Services'][14] . '&gm=' . $item['Services'][15] . '&gd=' . $item['Services'][16] . '&gp=' . $item['Services'][17] . '&vpn=' . $item['Services'][18] . '&WomenSecurity=' . $item['WomenSecurity'] . '"><h6>' . $item['Flag'] . $item['Name'] . '</h6></a>';
	}

?></div>
 </li>
 <li>
   <div class="collapsible-header"><i class="material-icons"><img src="img/southamerica.png" height="20" width="20"> </i>South America
   <span class="new badge green accent-3" data-badge-caption="countries"><?php
echo count($southamerica); ?></span></div>
   <div class="collapsible-body"><?php

foreach($southamerica as $item)
	{
	echo '<a class="collection-item" href="profile.php?Name=' . $item['Name'] . '&DemocracyIndex=' . $item['DemocracyIndex'] . '&Freedom=' . $item['Freedom'] . '&HomoLegalStatus=' . $item['HomoLegalStatus'] . '&HomoUnacceptable=' . $item['HomoUnacceptable'] . '&WomenDressCode=' . $item['WomenDressCode'] . '&WomenGenderGap=' . $item['WomenGenderGap'] . '&f=' . $item['Services'][0] . '&in=' . $item['Services'][1] . '&wha=' . $item['Services'][2] . '&tw=' . $item['Services'][3] . '&sn=' . $item['Services'][4] . '&tg=' . $item['Services'][5] . '&yt=' . $item['Services'][6] . '&net=' . $item['Services'][7] . '&hu=' . $item['Services'][8] . '&hbo=' . $item['Services'][9] . '&sp=' . $item['Services'][10] . '&am=' . $item['Services'][11] . '&ti=' . $item['Services'][12] . '&g=' . $item['Services'][13] . '&gma=' . $item['Services'][14] . '&gm=' . $item['Services'][15] . '&gd=' . $item['Services'][16] . '&gp=' . $item['Services'][17] . '&vpn=' . $item['Services'][18] . '&WomenSecurity=' . $item['WomenSecurity'] . '"><h6>' . $item['Flag'] . $item['Name'] . '</h6></a>';
	}

?></div>
 </li>
 <li>
   <div class="collapsible-header"><i class="material-icons"><img src="img/africa.png" height="20" width="20"> </i>Africa
   <span class="new badge light-blue accent-3" data-badge-caption="countries"><?php
echo count($africa); ?></span></div>
   <div class="collapsible-body"><?php

foreach($africa as $item)
	{
	echo '<a class="collection-item" href="profile.php?Name=' . $item['Name'] . '&DemocracyIndex=' . $item['DemocracyIndex'] . '&Freedom=' . $item['Freedom'] . '&HomoLegalStatus=' . $item['HomoLegalStatus'] . '&HomoUnacceptable=' . $item['HomoUnacceptable'] . '&WomenDressCode=' . $item['WomenDressCode'] . '&WomenGenderGap=' . $item['WomenGenderGap'] . '&f=' . $item['Services'][0] . '&in=' . $item['Services'][1] . '&wha=' . $item['Services'][2] . '&tw=' . $item['Services'][3] . '&sn=' . $item['Services'][4] . '&tg=' . $item['Services'][5] . '&yt=' . $item['Services'][6] . '&net=' . $item['Services'][7] . '&hu=' . $item['Services'][8] . '&hbo=' . $item['Services'][9] . '&sp=' . $item['Services'][10] . '&am=' . $item['Services'][11] . '&ti=' . $item['Services'][12] . '&g=' . $item['Services'][13] . '&gma=' . $item['Services'][14] . '&gm=' . $item['Services'][15] . '&gd=' . $item['Services'][16] . '&gp=' . $item['Services'][17] . '&vpn=' . $item['Services'][18] . '&WomenSecurity=' . $item['WomenSecurity'] . '"><h6>' . $item['Flag'] . $item['Name'] . '</h6></a>';
	}

?></div>
 </li>
 <li>
   <div class="collapsible-header"><i class="material-icons"><img src="img/oceania.png" height="20" width="20"> </i>Oceania
   <span class="new badge purple accent-3" data-badge-caption="countries"><?php
echo count($oceania); ?></span></div>
   <div class="collapsible-body"><?php

foreach($oceania as $item)
	{
	echo '<a class="collection-item" href="profile.php?Name=' . $item['Name'] . '&DemocracyIndex=' . $item['DemocracyIndex'] . '&Freedom=' . $item['Freedom'] . '&HomoLegalStatus=' . $item['HomoLegalStatus'] . '&HomoUnacceptable=' . $item['HomoUnacceptable'] . '&WomenDressCode=' . $item['WomenDressCode'] . '&WomenGenderGap=' . $item['WomenGenderGap'] . '&f=' . $item['Services'][0] . '&in=' . $item['Services'][1] . '&wha=' . $item['Services'][2] . '&tw=' . $item['Services'][3] . '&sn=' . $item['Services'][4] . '&tg=' . $item['Services'][5] . '&yt=' . $item['Services'][6] . '&net=' . $item['Services'][7] . '&hu=' . $item['Services'][8] . '&hbo=' . $item['Services'][9] . '&sp=' . $item['Services'][10] . '&am=' . $item['Services'][11] . '&ti=' . $item['Services'][12] . '&g=' . $item['Services'][13] . '&gma=' . $item['Services'][14] . '&gm=' . $item['Services'][15] . '&gd=' . $item['Services'][16] . '&gp=' . $item['Services'][17] . '&vpn=' . $item['Services'][18] . '&WomenSecurity=' . $item['WomenSecurity'] . '"><h6>' . $item['Flag'] . $item['Name'] . '</h6></a>';
	}

?></div>
 </li>
</ul>
    </div>
    <br /><br /><br />
    <script src='js/materialize.min.js'></script>
    <script  src="js/index.js"></script>

    <script>var elem = document.querySelector(".collapsible");
    var instance = new M.Collapsible(elem, {

    });</script>

  </body>
  <?php
include 'footer.php';
 ?>
</html>
