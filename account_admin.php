<html>
  <body>
    <h2>Premium Accounts Administration
    </h2>
    <?php
require 'vendor/autoload.php';
use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;
$sdk = new Aws\Sdk([
  'region'   => 'us-east-2',
  'version'  => 'latest'
  ]);
$dynamodb = $sdk->createDynamoDb();
$marshaler = new Marshaler();
$tableName = 'UsersPremium';
$eav = $marshaler->marshalJson('
  {
    ":ten": "'.$_SESSION['username'].'"
  }
  ');
$params = [
  'TableName' => $tableName,
  'KeyConditionExpression' => 'Tenant = :ten',
  'IndexName' => 'Tenant-index',
  'ExpressionAttributeValues'=> $eav
];
try {
  $result = $dynamodb->query($params);
  echo '<ul class="collection">';
  foreach ($result['Items'] as $user) {
    $item = $marshaler->unmarshalItem($user);
    echo '<li class="collection-item"><div>'.$item['Email'].'<a href="removeuser.php?user='.$item['Email'].'" class="secondary-content"><i class="material-icons">delete</i></a></div></li>';
  }
  echo '</ul>';
  } catch (DynamoDbException $e) {
    echo "Unable to query:\n";
    echo $e->getMessage() . "\n";
}
?>
    <form action="adduser.php" method="POST" id="adduser" enctype="multipart/form-data">
      <div class="row">
        <div class="input-field col s12 m7">
          <input id="newuser" type="email" name="newuser" class="validate">
          <label for="newuser">Email
          </label>
        </div>
        <div class="input-field col s12 m3">
          <button type="submit" form="adduser" class="btn waves-effect waves-light" name="action">Add user
            <i class="material-icons right">send
            </i>
          </button>
        </div>
        </form>
      </div>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.1/js/materialize.min.js'>
    </script>
    <script  src="js/index.js">
    </script>
  </body>
</html>
