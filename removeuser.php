<?php
  require 'vendor/autoload.php';

  session_start();

  if(!isset($_SESSION['username'])) {
      header('Location: login.php');
  }

  use Aws\DynamoDb\Exception\DynamoDbException;
  use Aws\DynamoDb\Marshaler;

  $sdk = new Aws\Sdk([
      'region'   => 'us-east-2',
      'version'  => 'latest'
    ]);



   $dynamodb = $sdk->createDynamoDb();
   $marshaler = new Marshaler();
//,"Tenant": "' . $tenant . '"
   $key = $marshaler->marshalJson('
            {
               "Email": "' . $_GET['user'] . '"

            }
        ');
 $params = ['TableName' => 'UsersPremium','Key' => $key];

        try {
          $result = $dynamodb->deleteItem($params);

          if (isset($_SESSION['premium']))
						{

              if($_SESSION['username']==$_GET['user']){

                $_SESSION['premium']=False;
                unset($_SESSION['services']);
              }

						}
          header("Location: account.php");

        } catch (DynamoDbException $e) {
          echo "Unable to remove item:\n";
          echo $e->getMessage() . "\n";
        }

?>
