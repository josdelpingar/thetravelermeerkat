<?php
session_start();
if (!isset($_SESSION['username'])) {
    header('Location: login.php');
}
include 'header.php';
?>

<html>
  <body>
  <br><br><br>
    <div class="container">

      <?php
$none = true;
if (isset($_SESSION['premium'])) {
    if ($_SESSION['premium'] == True) {
        include 'account_preferences.php';
        $none = false;
    }
}
if (isset($_SESSION['tenant'])) {
    if ($_SESSION['tenant'] == True) {
        include 'account_admin.php';
        $none = false;
    }
}
if ($none == true) {
    header('Location: premium.php');
}
?>
    </div>
    <br/><br/><br/>
  </body>
  <?php include 'footer.php'; ?>
</html>
