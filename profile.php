<?php
session_start();

if (!isset($_SESSION['username']))
{
    header('Location: login.php');
}


include 'header.php';

$di = $_GET['DemocracyIndex'];
$f = $_GET['Freedom'];
$hls = $_GET['HomoLegalStatus'];
$hu = $_GET['HomoUnacceptable'];
$wdc = $_GET['WomenDressCode'];
$wgg = $_GET['WomenGenderGap'];
$ws = $_GET['WomenSecurity'];

$fac = $_GET['f'];
$tw = $_GET['tw'];
$wha = $_GET['wha'];
$sn = $_GET['sn'];
$in = $_GET['in'];
$tg = $_GET['tg'];
?>
<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile data-collapsible="accordion"-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <style>
      #progress-bar-container {
	        width: 200px;
	        height: 20px;
	        transform: translateY(-50%);

        }

      .progress-bar-child {
	         width: 100%;
	         height: 100%;
        }

      .progress1 {
	       color: white;
	       text-align: center;
	       line-height: 75px;
	       font-size: 35px;
	       font-family: "Segoe UI";
	       animation-direction: reverse;
	       background: #e5405e;
         background: linear-gradient(to right, #ca5bff 0%, #5d9eff 50%, #3effb4 100%);
       }

       .progress2 {
 	       color: white;
 	       text-align: center;
 	       line-height: 75px;
 	       font-size: 35px;
 	       font-family: "Segoe UI";
 	       animation-direction: reverse;
 	       background: #e5405e;
 	       background: linear-gradient(to right, #e5405e 0%, #ffdb3a 25%, #3fffa2 50%, #3fffa2 50%, #1a9be0 73%, #ba68ed 100%);
        }

        .progress3 {
  	       color: white;
  	       text-align: center;
  	       line-height: 75px;
  	       font-size: 35px;
  	       font-family: "Segoe UI";
  	       animation-direction: reverse;
  	       background: #e5405e;
  	       background: linear-gradient(to right, #e5405e 0%, #f17dff 100%);
         }


       .shrinker {
	        background-color: black;
	         position: absolute;
	          top: 0;
	           right: 0;
	            width: 100%;
          }



        .timelapse {
	           animation-name: timelapse;
	           animation-fill-mode: forwards;
	           animation-duration: 2s;
	           animation-timing-function: cubic-bezier(.86, .05, .4, .96);
        }

        @keyframes timelapse {
	         0% {
		           width: 100%;
	            }
	         100% {
		           width: <?php echo (10 - $di) * 10; ?>%;
	            }
        }
        .timelapse2 {
	           animation-name: timelapse2;
	           animation-fill-mode: forwards;
	           animation-duration: 2s;
	           animation-timing-function: cubic-bezier(.86, .05, .4, .96);
        }

        @keyframes timelapse2 {
	         0% {
		           width: 100%;
	            }
	         100% {
		           width: <?php echo $hu; ?>%;
	            }
        }

        .timelapse3 {
	           animation-name: timelapse3;
	           animation-fill-mode: forwards;
	           animation-duration: 2s;
	           animation-timing-function: cubic-bezier(.86, .05, .4, .96);
        }

        @keyframes timelapse3 {
	         0% {
		           width: 100%;
	            }
	         100% {
		           width: <?php echo 100 - $wgg; ?>%;
	            }
        }

    </style>



  </head>
  <body class="grey lighten-4">

      <div class="container">

        <div class="row">
          <div class="col s12 m9 l10" id="bodybox">
            <br/>
            <?php
            if (isset($_SESSION['services']))
            {
                  include 'unavailable_services.php';
            }

           ?>
          <div id="freedom" class="section scrollspy">
          <span><h1><b>FREEDOM 🕊</h1></b></span><br>
          <span><h5><b>DEMOCRACY INDEX</b></h5></span><br>
          <div id="progress-bar-container">
		          <div class="progress-bar-child progress1 tooltipped z-depth-2" data-position="right" data-tooltip="<?php echo $di; ?>"></div>
		            <div class="progress-bar-child shrinker timelapse tooltipped z-depth-2" data-position="right" data-tooltip="<?php echo $di; ?>"></div>
          </div>
          <span><h5><b>FREEDOM INDEX</b></h5></span><br>
          <?php
if ($f == 2) echo '<a class="waves-effect waves-light btn-small light-green">democracy</a>';
if ($f == 1) echo '<a class="waves-effect waves-light btn-small amber accent-3">mixed regime</a><br><br>';
if ($f == 0) echo '<a class="waves-effect waves-light btn-small red">dictatorship</a>';
?>
        </div>
        <br><br><br>
        <div class="divider"></div>
          <div id="lgbtrights" class="section scrollspy">
          <span><h1><b>LGBT RIGHTS 🌈</b></h1></span><br>
          <span><h5><b>HOMOSEXUALITY LEGAL STATUS</b></h5></span><br>
          <?php
if ($hls == 0) echo '<a class="waves-effect waves-light btn-small light-blue ">equal marriage</a>';
if ($hls == 1) echo '<a class="waves-effect waves-light btn-small green ">equal marriage recognized</a>';
if ($hls == 2) echo '<a class="waves-effect waves-light btn-small lime">civil unions</a>';
if ($hls == 3) echo '<a class="waves-effect waves-light btn-small yellow darken-2">no type of union recognized</a>';
if ($hls == 4) echo '<a class="waves-effect waves-light btn-small amber darken-2">freedom of expression and association restricted</a>';
if ($hls == 5) echo '<a class="waves-effect waves-light btn-small orange ">imprisonment</a>';
if ($hls == 6) echo '<a class="waves-effect waves-light btn-small deep-orange ">up to life sentence</a>';
if ($hls == 7) echo '<a class="waves-effect waves-light btn-small red ">up to death sentence</a>';
?>
          <br><br>
          <span><h5><b>% OF HOMOSEXUALITY ACCEPTANCE AMONG CITIZENS</b></h5></span><br>

          <div id="progress-bar-container">
		          <div class="progress-bar-child progress2 tooltipped z-depth-2" data-position="right" data-tooltip="<?php echo 100 - $hu; ?>%"></div>
		            <div class="progress-bar-child shrinker timelapse2 tooltipped z-depth-2" data-position="right" data-tooltip="<?php echo 100 - $hu; ?>%"></div>
          </div>
        </div>
        <br><br><br>
        <div class="divider"></div>
        <div id="womenrights" class="section scrollspy">
          <span ><h1><b>WOMEN RIGHTS 🙋🏽</b></h1></span><br>
          <span ><h5><b>WOMEN REQUIRED DRESSCODE</b></h5></span>

          <div class="row">
          <div class="col s1"><h2 class="tooltipped" data-position="right" data-tooltip="<?php
if ($wdc == 0) echo 'Women can dress freely';
if ($wdc == 1) echo 'Women required to satisfy certain dress code';
if ($wdc == 2) echo 'Women required to wear hijab or veil';
?>">

            <?php
if ($wdc == 0) echo '💁🏽‍♀️';
if ($wdc == 1) echo '🙎🏽‍♀️';
if ($wdc == 2) echo '🧕🏽';
?>

          </h2></div>
        <div class="col s11"></div>
        </div>


          <span ><h5><b>WOMEN GENDER GAP</b></h5></span><br>

          <div id="progress-bar-container">
		          <div class="progress-bar-child progress3 tooltipped z-depth-2" data-position="right" data-tooltip="There is a gap of <?php echo 100 - $wgg; ?>%"></div>
		            <div class="progress-bar-child shrinker timelapse3 tooltipped z-depth-2" data-position="right" data-tooltip="There is a gap of <?php echo 100 - $wgg; ?>%"></div>
          </div>

          <span ><h5><b>WOMEN SECURITY</b></h5></span><br>
          <?php
if ($ws == 0) echo '<a class="waves-effect waves-light btn-small light-blue ">secure</a>';
if ($ws == 1) echo '<a class="waves-effect waves-light btn-small green ">high level of security</a>';
if ($ws == 2) echo '<a class="waves-effect waves-light btn-small yellow darken-2">moderate level of security</a>';
if ($ws == 3) echo '<a class="waves-effect waves-light btn-small orange ">low level of security</a>';
if ($ws == 4) echo '<a class="waves-effect waves-light btn-small red ">lack of security</a>';
?>
          <br>
        </div>
        </div>
          <div id="navy" class="col hide-on-small-only m3 l2">
            <ul id="toc" class="section table-of-contents">
              <li>
                <a href="#freedom"><span >FREEDOM</span></a>
              </li>
              <li>
                <a href="#lgbtrights"><span >LGBT RIGHTS</span></a>
              </li>
              <li>
                <a href="#womenrights"><span >WOMEN RIGHTS</span></a>
              </li>
            </ul>
          </div>
          </div>
        </div>
        <br><br><br>

      <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js'></script>
      <script  src="js/index.js"></script>

    </body>
    <?php include 'footer.php'; ?>
</html>
