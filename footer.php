<footer class="page-footer cyan">
      <div class="container">
        <div class="row">
          <div class="col l6 s12">
            <h5 class="white-text">Our goal.</h5>
            <p class="grey-text text-lighten-4">The Traveler Meerkat was born to make travelling a safer adventure by making information about human rights more accesible.</p>
          </div>
          <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Links</h5>
            <ul>
              <li><a class="grey-text text-lighten-3" href="https://infographics.economist.com/2018/DemocracyIndex/">Democracy Index</a></li>
              <li><a class="grey-text text-lighten-3" href="https://freedomhouse.org/report/freedom-world/freedom-world-2018">Fredom Index</a></li>
              <li><a class="grey-text text-lighten-3" href="https://commons.wikimedia.org/wiki/File:World_laws_pertaining_to_homosexual_relationships_and_expression.svg">Homosexuality Legal Status</a></li>
              <li><a class="grey-text text-lighten-3" href="http://reports.weforum.org/global-gender-gap-report-2017/dataexplorer/">Gender Gap</a></li>
              <li><a class="grey-text text-lighten-3" href="http://chartsbin.com/view/38545">Women's Physical Security</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container">
        © 2018 The Traveler Meerkat
        <a class="grey-text text-lighten-4 right" href="premium.php">Become a tenant</a>
        </div>
      </div>
    </footer>
    <?php if (isset($_SESSION['username']))
    	{
    	echo '<div class="fixed-action-btn">
    <a href="account.php" class="btn-floating btn-large red lighten-2">
      <i class="large material-icons">person</i>
    </a>';
    	} ?>

</div>
